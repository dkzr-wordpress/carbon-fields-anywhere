Carbon Fields Anywhere
=======================================================================

PHP library to  complement the Carbon Fields library. You should install this library using [composer](https://getcomposer.org).

When Carbon Fields is installed as a vendor outside the webroot, make assets available.

When using composer to manage your site dependencies and you require a theme or plugin that requires "htmlburger/carbon-fields" the Carbon Fields code may be installed in a vendors directory outside your webroot. Then the Carbon Fields assets are not available online.

This library fixes this by rewriting the Carbon Fields assets urls and serving the files through PHP.

The library will init Carbon Fields by calling `Carbon_Fields\Carbon_Fields::boot()`

## Usage

Add `CarbonFieldsAnywhere::boot();` to either te WordPress "plugins_loaded" or "after_setup_theme" hook.

```php
add_action( 'after_setup_theme', function() {
  if ( class_exists( 'CarbonFieldsAnywhere' ) ) {
    CarbonFieldsAnywhere::boot();
  }
}, 10, 0 );
```
