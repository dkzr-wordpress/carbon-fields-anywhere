<?php
/**
 * Carbon Fields Anywhere
 *
 * Checks if Carbon Fields is installed outside the webroot and handles accordingly.
 * Runs Carbon_Fields\Carbon_Fields::boot()
 *
 * Version: 2.1.2
 * Description: When Carbon Fields is installed as a vendor outside the webroot, make assets available.
 * Author: Joost de Keijzer
 * Author URI: https://dkzr.nl
 */

use Carbon_Fields\Carbon_Fields as CarbonFields;
use Carbon_Fields\Container as Container;
use Carbon_Fields\Field as Field;

class CarbonFieldsAnywhere {
  protected static $instance = null;
  protected $query_var = 'carbon-fields-asset';

  public static function boot() {
    if ( class_exists( '\Carbon_Fields\Carbon_Fields' ) ) {
      if ( is_null( self::$instance ) ) {
        self::$instance = new CarbonFieldsAnywhere();
      }

      CarbonFields::boot();
    }
  }

  public function __construct() {
    $reflector = new \ReflectionClass('\Carbon_Fields\Carbon_Fields');
    $dir = dirname( dirname( $reflector->getFileName() ) );

    if ( '' == \Carbon_Fields\Carbon_Fields::directory_to_url( $dir ) ) {
      $this->setup_outside_webroot();
    }
  }

  protected function setup_outside_webroot() {
    if ( defined( 'Carbon_Fields\URL' ) ) {
      // too late... maybe `Carbon_Fields\Carbon_Fields::boot()` was already run?
      _doing_it_wrong( 'CarbonFieldsAnywhere::boot', '`CarbonFieldsAnywhere::boot()` should be called only once and replaces `Carbon_Fields\Carbon_Fields::boot()`. Call on either the "plugins_loaded" or "after_setup_theme" hook.', '1.0' );
      return;
    }

    define( 'Carbon_Fields\URL', site_url( "?{$this->query_var}=" ) );

    add_filter( 'query_vars', [ $this, 'query_vars' ], 10, 1 );

    add_action( 'parse_request', [ $this, 'parse_request' ], -10, 1 );
  }

  public function query_vars( $query_vars ) {
    $query_vars[] = $this->query_var;
    return $query_vars;
  }

  public function parse_request( $wp ) {
    $file_url = isset( $wp->query_vars[ $this->query_var ] ) && is_string( $wp->query_vars[ $this->query_var ] ) ? $wp->query_vars[ $this->query_var ] : '';

    // Not a "carbon-fields-outside-webroot-fix" url or Carbon_Fields\DIR not defined
    // return and continue WordPress
    if ( empty( $file_url ) || ! defined( 'Carbon_Fields\DIR' ) ) {
      return;
    }

    $file = realpath( trailingslashit( Carbon_Fields\DIR ) . $file_url );

    // File outside Carbon_Fields\DIR or PHP file
    // return and let WordPress handle it
    if (
      trailingslashit( Carbon_Fields\DIR ) != substr( $file, 0, strlen( trailingslashit( Carbon_Fields\DIR ) ) )
      || '.php' == substr( $file, -4 )
    ) {
      return;
    }

    // Carbon Fields asset file doesn't exist
    // exit 404
    if ( ! is_file( $file ) ) {
      status_header( 404 );
      exit( '404 &#8212; File not found.' );
    }

    // code below taken from wp-includes/ms-files.php
    error_reporting( 0 );

    $mime = wp_check_filetype( $file, wp_get_mime_types() );
    if ( false === $mime['type'] && function_exists( 'mime_content_type' ) ) {
      $mime['type'] = mime_content_type( $file );
    }

    if ( $mime['type'] ) {
      $mimetype = $mime['type'];
    } else {
      $mimetype = 'image/' . substr( $file, strrpos( $file, '.' ) + 1 );
    }

    header( 'Content-Type: ' . $mimetype ); // always send this
    if ( false === strpos( $_SERVER['SERVER_SOFTWARE'], 'Microsoft-IIS' ) ) {
      header( 'Content-Length: ' . filesize( $file ) );
    }

    if ( is_multisite() ) {
      ms_file_constants();
      // Optional support for X-Sendfile and X-Accel-Redirect
      if ( WPMU_ACCEL_REDIRECT ) {
        header( 'X-Accel-Redirect: ' . str_replace( WP_CONTENT_DIR, '', $file ) );
        exit;
      } elseif ( WPMU_SENDFILE ) {
        header( 'X-Sendfile: ' . $file );
        exit;
      }
    }

    $last_modified = gmdate( 'D, d M Y H:i:s', filemtime( $file ) );
    $etag          = '"' . md5( $last_modified ) . '"';
    header( "Last-Modified: $last_modified GMT" );
    header( 'ETag: ' . $etag );
    header( 'Expires: ' . gmdate( 'D, d M Y H:i:s', time() + 100000000 ) . ' GMT' );

    // Support for Conditional GET - use stripslashes to avoid formatting.php dependency
    $client_etag = isset( $_SERVER['HTTP_IF_NONE_MATCH'] ) ? stripslashes( $_SERVER['HTTP_IF_NONE_MATCH'] ) : false;

    if ( ! isset( $_SERVER['HTTP_IF_MODIFIED_SINCE'] ) ) {
      $_SERVER['HTTP_IF_MODIFIED_SINCE'] = false;
    }

    $client_last_modified = trim( $_SERVER['HTTP_IF_MODIFIED_SINCE'] );
    // If string is empty, return 0. If not, attempt to parse into a timestamp
    $client_modified_timestamp = $client_last_modified ? strtotime( $client_last_modified ) : 0;

    // Make a timestamp for our most recent modification...
    $modified_timestamp = strtotime( $last_modified );

    if ( ( $client_last_modified && $client_etag )
      ? ( ( $client_modified_timestamp >= $modified_timestamp ) && ( $client_etag == $etag ) )
      : ( ( $client_modified_timestamp >= $modified_timestamp ) || ( $client_etag == $etag ) )
    ) {
      status_header( 304 );
      exit;
    }

    // If we made it this far, just serve the file

    // disable all output buffering
    while ( @ob_end_flush() );

    readfile( $file );
    flush();
    exit;
  }
}
